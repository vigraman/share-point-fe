const express = require('express')
const path = require('path')
const parser = require('body-parser')
const app = express()
const fileUpload = require('express-fileupload')

global.APP_PATH = path.join(__dirname, './')
const middleware = require('webpack-dev-middleware')
const webpack = require('webpack')
const config = require('./webpack.config')
const compiler = webpack(config)


app.use(middleware(compiler, {
  publicPath: '/',
  stats: { colors: true },
  log: console.log,
  serverSideRender: true
}))

app.use(require('webpack-hot-middleware')(compiler))

app.use('/', express.static('public', {
  maxAge: 31536000000,
  setHeaders: function (res, path) {
    res.setHeader('Expires', new Date(Date.now() + 2592000000 * 30).toUTCString())
  }
}))

app.use(fileUpload({
  useTempFiles: true,
  tempFileDir: "./tmp/"
}))

app.use(parser.urlencoded({limit: '100mb', extended : true}));

const uploadS3 = (req, res, isPublic) => {
  return new Promise((resolve, reject) => {
    const uploadedFile = req.files
    if (uploadedFile) {
      const s3Lib = require('./src/s3')
      const s3Key = `${Date.now()}_${uploadedFile.file.name}`
      s3Lib.upload(uploadedFile, s3Key, isPublic).then(data => {
        resolve(data)
      }).catch((err) => {
        console.log(err)
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({
          CODE: 'ERROR_IN_UPLOAD'
        })
      })
    }
  })
}

app.post('/uploadS3/public', async (req, res) => {
    const resp = await uploadS3(req, res, true)
    res.send(resp)
})


app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '/public', 'index.html'))
})
  
app.listen(3000, () => console.log('App listening on port 3000!'))
  