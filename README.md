#Share Point Front-end
---
This is about upload file to cloud and sharing the file to people who all are connected


##Installation
> git clone git@gitlab.com:vigraman/share-point-fe.git
> cd share-point-fe
> npm install

###To Start Project
> npm start

####To Visit App
> http://localhost:3000 

####Folder Structure
share-point-fe
│   README.md
│   server.js
│   index.js 
│   package.json
│   webpack.config.js
│   .gitignore
│
└───src
│   │   s3.js
│   │   index.js
│   │   routes.js
│   │
│   └───app
│   │   │   App.jsx
│   │   └───component
│   │   │   └───Header
│   │   │           Header.css
│   │   │           Header.jsx
│   │   │   └───Home
│   │   │           Home.css
│   │   │           Home.jsx
│   │   │   └───Login
│   │   │           Login.css
│   │   │           Login.jsx
│   │   │   └───UploadContainer
│   │   │           UploadContainer.css
│   │   │           UploadContainer.jsx
│   │   │   └───ViewFile
│   │   │           ViewFile.jsx
│   │   │  
│   └───config
│   │       config.json
│   │       index.js
│   └───helpers
│   │       global.js
│   │       utils.js
│   └───services
│   │       api.js
