import { create } from 'apisauce'
import { USER_TOKEN_STRING } from '../helpers/global'
const env = process.env.NODE_ENV || 'development'
const config = require('../config/config.json')

const DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
  'cache-control': 'no-cache',
  'Authentication': `Bearer ${localStorage.getItem(USER_TOKEN_STRING)}`
}

const api = create({
  baseURL: config[env].backend.url,
  headers: {
    ...DEFAULT_HEADERS
  },
  timeout: 5000
})

const local = create({
    baseURL: '/'
})

export const setHeaders = (headers = {}) => {
    api.setHeaders({ ...DEFAULT_HEADERS, ...headers })
}

export const login = (data = {}) => api.post('/api/user/login', data);
export const uploadFileUrl = (data = {}) => api.post('/api/file/upload', data);

export const uploadToS3Public = (formData = {}) => local.post('/uploadS3/public', formData)