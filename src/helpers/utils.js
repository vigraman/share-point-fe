import { EMAIL_REGEX, PWD_REGEX } from "./global";
export const validateEmail = mail => {
  if (
    EMAIL_REGEX.test(mail)
  ) {
    return false;
  }
  return true;
};

export const passwordValidate = pwd => {
  if(PWD_REGEX.test(pwd)) {
    return false
  } else {
    return true
  }
}