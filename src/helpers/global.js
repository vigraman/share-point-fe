
export const USER_TOKEN_STRING = 'user_token'

export const PWD_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{15,}$/

export const EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

export const FILE_TYPE = {
    pdf: 'application/pdf',
    msword: 'application/msword',
    jpeg: 'image/jpeg'
}