import React from 'react'
import './Login.css'
import {validateEmail, passwordValidate} from '../../../helpers/utils'
import {login} from '../../../services/api'
import { USER_TOKEN_STRING } from '../../../helpers/global'
class Login extends React.Component {

    state = {
        email: '',
        password: '',
        emailValidateError: false,
        pwdValidateError: false
    }

    handleChange = (key, value) => {
        this.setState({[key]: value});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let {emailValidateError, pwdValidateError, email, password} = this.state;
        emailValidateError = validateEmail(email);
        pwdValidateError = passwordValidate(password);
        if(emailValidateError || pwdValidateError) {
            this.setState({emailValidateError, pwdValidateError})
        } else {
            const payload = {
                email: email,
                password: password
            }
            login(payload).then(data => {
                const {data: resData} = data;
                const { body, status } = resData;
                if(status === 200) {
                    localStorage.setItem(USER_TOKEN_STRING, body.accessToken)
                    this.props.loggedIn();
                }
            }).catch(err => {
                console.log(err);
            })
        }
    }

    render() {
        const {emailValidateError, pwdValidateError} = this.state;
        return(
            <div className='wrapper'>
                <div className='form-wrapper'>
                <h2>Login</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className='email'>
                        <label htmlFor="email">Email</label>
                        <input type='email' name='email' onChange={(e) => this.handleChange('email', e.target.value)}/>
                        <span className="error">{emailValidateError ? "Email Doesn't meet the requirement": null}</span>
                    </div>
                    <div className="password">
                        <label htmlFor="email">Password</label>
                            <input type='password' name='password' onChange={(e) => this.handleChange('password', e.target.value)}/>
                            <span className="error">{pwdValidateError? "Password Should be 1-Uppercase, 1-Lowercase, symbol, 1-Number and the length is 15": null}</span>
                    </div>
                    <div className='submit'>
                        <button className="create-btn">Login</button>
                    </div>
                </form>
                </div>
            </div>
        )
    }
}

export default Login;