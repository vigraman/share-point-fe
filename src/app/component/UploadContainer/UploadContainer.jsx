import React from "react";
import "./UploadContainer.css";
import { uploadToS3Public } from "../../../services/api";

class UploadContainer extends React.Component {
  componentDidMount() {
    this.dragDropInit();
  }

  dragDropInit() {
    const dropWrapper = document.getElementById("dropzone-wrapper");
    dropWrapper.ondragover = (e) => {
      e.preventDefault();
      e.stopPropagation();
      dropWrapper.classList.add("dragover");
    };
    dropWrapper.ondragleave = (e) => {
      e.preventDefault();
      e.stopPropagation();
      dropWrapper.removeClass("dragover");
    };
  }

  uploadDocument = (input) => {
    const file = input.target.files && input.target.files[0];
    if(file) {
        const fd = new FormData();
        fd.append('file', file)
        uploadToS3Public(fd).then(data => {
          const resp = {
            type: file.type.split("/")[1],
            url: data.data.url
          }
          this.props.setS3Url(resp)
        }).catch(err => {
            console.log(err)
        })
    }
  }

  render() {
    return (
      <div className="dropzone-wrapper" id="dropzone-wrapper">
        <div className="dropzone-desc">
          <i className="glyphicon glyphicon-download-alt"></i>
          <p>Choose an file or drag it here.</p>
        </div>
        <input
          type="file"
          name="file"
          onChange={this.uploadDocument}
          accept="application/msword, application/pdf, image/jpeg"
          className="dropzone"
          id="dropzone-input"
        />
      </div>
    );
  }
}

export default UploadContainer;
