import React from 'react'
import './Header.css'
import { USER_TOKEN_STRING } from '../../../helpers/global'

class Header extends React.Component {

    logout = () => {
        localStorage.removeItem(USER_TOKEN_STRING)
        this.props.logout();
    }

    render() {
        return (
            <div className='header-wrapper'>
                <div onClick={this.logout}>
                    Logout
                </div>
            </div>
        )
    }
}

export default Header;