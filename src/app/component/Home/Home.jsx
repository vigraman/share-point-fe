import React from 'react'
import Header from '../Header/Header'
import UploadConatiner from '../UploadContainer/UploadContainer'
import { FILE_TYPE, USER_TOKEN_STRING } from '../../../helpers/global'
import { uploadFileUrl } from '../../../services/api'
import Login from '../Login/Login'
class Home extends React.Component {
    state = {
        listOfFile: [],
        currentFile: '',
        loggedIn: localStorage.getItem(USER_TOKEN_STRING) ? true : false
    }

    setS3Url = (data) => {
        uploadFileUrl(data).then(data => {
            console.log(data)
        }).catch(err => {
            
        })
        this.setState({currentFile: data})
    }
    loggedIn = () => {
        this.setState({loggedIn: true});
    }

    logout = () => {
        this.setState({loggedIn: false});
    }

    render() {
        const {currentFile, loggedIn} = this.state;
        if(loggedIn) {
            return (
                <div>
                    <Header logout={this.logout}/>
                    <UploadConatiner setS3Url={this.setS3Url}/>
                    <embed src={currentFile.url} accept={FILE_TYPE[currentFile.type]} width="100" height="175"/>
                </div>
            )
        } else {
            return (
                <Login loggedIn={this.loggedIn}/>
            )
        }
    }
}

export default Home;