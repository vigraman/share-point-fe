import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Login from './app/component/Login/Login'
import Home from './app/component/Home/Home'

export const Routes = (routeProps) => {
    return (
        <Switch>
            <Route path='/' exact component={(props) => <Home {...props} {...routeProps}/>} />
        </Switch>
    )

}
